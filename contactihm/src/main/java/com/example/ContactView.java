package com.example;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class ContactView extends JPanel implements ActionListener{

    private Contact model;
    private JLabel label;
    private JButton button;

    public ContactView(Contact c){
        super();
        setLayout(new GridBagLayout());
        model= c;
        label= new JLabel(c.toString()+"  ");
        button= new JButton("editer");
        button.setSize(30, 20);
        button.addActionListener(this);
        add(label);
        add(button);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ContactForm.getInstance(model);
    }
    
}
