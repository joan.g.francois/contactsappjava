package com.example;

import java.util.ArrayList;

public interface IRequestContacts {

    public void receiveContact(ArrayList<Contact> contacts);
    public void receiveContact(Contact contact);
}
