package com.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ServiceAPI {
    public static String API_URL= "http://localhost:8080/api/";

    public static void getContacts(IRequestContacts listener) throws IOException {
        String requestReponse= requestHTTP("GET", "contact");
        JsonArray jsonArray= JsonParser.parseString(requestReponse).getAsJsonArray();
        ArrayList<Contact> contacts= new ArrayList<Contact>();
        for(int i=0; i< jsonArray.size(); i++){
            JsonObject jsonObject= jsonArray.get(i).getAsJsonObject();
            Contact model= ContactHelper.jsonToContact(jsonObject);
            contacts.add(model);
        }
        listener.receiveContact(contacts);
    }

    public static void saveContact(Contact contact, IRequestContacts listener)throws IOException{
        String data = ContactHelper.contactToJson(contact);
        String requestReponse= requestHTTP("POST", "contact", data);
        JsonObject jsonObject= JsonParser.parseString(requestReponse).getAsJsonObject();
        Contact model= ContactHelper.jsonToContact(jsonObject);
        listener.receiveContact(model);
    }

    public static void updateContact(Contact contact, IRequestContacts listener)throws IOException{
        String data = ContactHelper.contactToJson(contact);
        String requestReponse= requestHTTP("PUT", "contact/"+contact.getId(), data);
        JsonObject jsonObject= JsonParser.parseString(requestReponse).getAsJsonObject();
        Contact model= ContactHelper.jsonToContact(jsonObject);
        listener.receiveContact(model);
    }

    public static void deleteContact(Contact contact, IRequestContacts listener)throws IOException{
        requestHTTP("DELETE", "contact/"+contact.getId());
        listener.receiveContact(new Contact());
    }

    private static String requestHTTP(String method, String ressource) throws IOException{
        return requestHTTP(method, ressource, null);
    }

    private static String requestHTTP(String method, String ressource, String data) throws IOException{
        URL obj = new URL(API_URL+ressource);
        HttpURLConnection httpURLConnection = (HttpURLConnection) obj.openConnection();
        httpURLConnection.setRequestMethod(method);
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        if(data!=null){
            httpURLConnection.setDoOutput(true);
            try(OutputStream os = httpURLConnection.getOutputStream()) {
                byte[] input = data.getBytes("utf-8");
                os.write(input, 0, input.length);			
            }
        }
        try(BufferedReader br = new BufferedReader(
            new InputStreamReader(httpURLConnection.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            return response.toString();
        }          
    }
}
