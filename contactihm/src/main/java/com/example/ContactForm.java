package com.example;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ContactForm extends JFrame implements ActionListener{

    private Contact contact;
    private JLabel labelFirstname, labelLastname, labelEmail, labelTel;
    private JTextField fieldFirstname, fieldLastname, fieldEmail, fieldTel;
    private JButton buttonSave, buttonDelete, buttonCancel;

    private static ContactForm instance;

    public static ContactForm getInstance(){
        if(instance==null){
            new ContactForm();
        }else{
            instance.contact= null;
        }
        return instance;
    }

    public static ContactForm getInstance(Contact c){
        if(instance==null){
            new ContactForm(c);
        }else{
            instance.contact= c;
        }
        return instance;
    }

    private ContactForm(){
        super();
        this.init();
    }

    private ContactForm(Contact c){
        super();
        contact= c;
        this.init();
    }

    private void init(){
        setVisible(true);
        setSize(300, 200);
        setLayout(new GridBagLayout());
        GridBagConstraints constraints= new GridBagConstraints();
        constraints.gridx = 0; 
        constraints.gridy = 0;
        constraints.weightx=0;
        constraints.fill=0;
        labelLastname= new JLabel("Nom : ");
        add(labelLastname, constraints);
        constraints.gridx = 1;
        constraints.weightx=1;
        constraints.fill=1;
        fieldLastname= new JTextField();
        add(fieldLastname, constraints);
        
        constraints.gridx = 0; 
        constraints.gridy = 1;
        constraints.weightx=0;
        constraints.fill=0;
        labelFirstname= new JLabel("Prénom : ");
        add(labelFirstname, constraints);
        constraints.gridx = 1;
        constraints.weightx=1;
        constraints.fill=1;
        fieldFirstname= new JTextField();
        add(fieldFirstname, constraints);

        constraints.gridx = 0; 
        constraints.gridy = 2;
        constraints.weightx=0;
        constraints.fill=0;
        labelEmail= new JLabel("Email : ");
        add(labelEmail, constraints);
        constraints.gridx = 1;
        constraints.weightx=1;
        constraints.fill=1;
        fieldEmail= new JTextField();
        add(fieldEmail, constraints);

        constraints.gridx = 0; 
        constraints.gridy = 3;
        constraints.weightx=0;
        constraints.fill=0;
        labelTel= new JLabel("Tel : ");
        add(labelTel, constraints);
        constraints.gridx = 1;
        constraints.weightx=1;
        constraints.fill=1;
        fieldTel= new JTextField();
        add(fieldTel, constraints);

        buttonSave = new JButton("Enregistrer");
        buttonSave.addActionListener(this);
        constraints.gridx = 0; 
        constraints.gridy = 4;
        constraints.weightx=0;
        constraints.fill=0;
        add(buttonSave, constraints);

        if(contact!=null){
            fieldLastname.setText(contact.getLastname());
            fieldFirstname.setText(contact.getFirstname());
            fieldEmail.setText(contact.getEmail());
            fieldTel.setText(contact.getTel());

            buttonDelete=new JButton("Supprimer");
            buttonDelete.addActionListener(this);
            constraints.gridx = 1; 
            constraints.anchor = GridBagConstraints.WEST;
            add(buttonDelete, constraints);
        }

        buttonCancel= new JButton("Annuler");
        buttonCancel.addActionListener(this);
        constraints.gridx = contact!=null ? 2 : 1; 
        constraints.anchor = GridBagConstraints.WEST;
        add(buttonCancel, constraints);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if(e.getSource().equals(buttonDelete)){
                ServiceAPI.deleteContact(contact, ContactAppView.getInstance());
                dispose();
            }else if(!e.getSource().equals(buttonCancel)){
                if(this.contact==null){
                    Contact tempContact= new Contact(fieldFirstname.getText(),
                                                fieldLastname.getText(),
                                                fieldEmail.getText(),
                                                fieldTel.getText());
                    ServiceAPI.saveContact(tempContact, ContactAppView.getInstance());
                }else{
                    contact.setFirstname(fieldFirstname.getText());
                    contact.setLastname(fieldLastname.getText());
                    contact.setEmail(fieldEmail.getText());
                    contact.setTel(fieldTel.getText());
                    ServiceAPI.updateContact(contact, ContactAppView.getInstance());
                }
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        dispose();
    }
    
}
