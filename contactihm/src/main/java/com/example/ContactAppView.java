package com.example;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.*;

public class ContactAppView extends JFrame implements IRequestContacts, ActionListener{

    GridBagConstraints constraints;
    JPanel panelContacts;
    JButton buttonAdd;

    private static ContactAppView instance;

    public static ContactAppView getInstance(){
        if(instance==null){
            instance= new ContactAppView();
        }
        return instance;
    }

    private ContactAppView(){
        super("Mes contacts");
        setVisible(true);
        setSize(600, 400);
        setLayout(new GridBagLayout());
        panelContacts= new JPanel();
        panelContacts.setLayout(new GridBagLayout());
        // Crée un objet de contraintes
        constraints= new GridBagConstraints();
        constraints.anchor = GridBagConstraints.NORTH;
        constraints.gridx = 0; 
        constraints.gridy = 0; 
        constraints.weighty=1;
        add(panelContacts, constraints); 
        constraints.anchor = GridBagConstraints.EAST;
        constraints.gridy = 1;
        constraints.weighty=1;
        buttonAdd = new JButton("nouveau");
        buttonAdd.setAlignmentX(CENTER_ALIGNMENT);
        buttonAdd.addActionListener(this);
        add(buttonAdd, constraints);
        try {
            ServiceAPI.getContacts(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

    @Override
    public void receiveContact(ArrayList<Contact> contacts) {
        constraints.gridx = 0; 
        constraints.gridy = 0;
        panelContacts.removeAll();
        constraints.anchor = GridBagConstraints.NORTH;
        for(Contact c : contacts){
            ContactView contactView= new ContactView(c);
            panelContacts.add(contactView, constraints);
            constraints.gridy++; 
        }
        validate();
        repaint();
    }

    @Override
    public void receiveContact(Contact contact) {
        try {
            ServiceAPI.getContacts(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ContactForm.getInstance();
    }
}
