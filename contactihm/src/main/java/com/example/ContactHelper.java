package com.example;

import com.google.gson.JsonObject;

public class ContactHelper {
    
    public static Contact jsonToContact(JsonObject jsonObject){
        return new Contact(jsonObject.get("id").getAsInt(), 
                        jsonObject.get("firstname").getAsString(),
                        jsonObject.get("lastname").getAsString(),
                        jsonObject.get("email").getAsString(),
                        jsonObject.get("tel").getAsString());
    }

    public static String contactToJson(Contact contact){
        return "{\"firstname\":\""+contact.getFirstname()+"\", "
                    +"\"lastname\": \""+contact.getLastname()+"\","
                    +"\"email\": \""+contact.getEmail()+"\","
                    +"\"tel\": \""+contact.getTel()+"\"}";
    }
}
