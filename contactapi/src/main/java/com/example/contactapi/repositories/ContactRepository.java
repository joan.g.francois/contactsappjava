package com.example.contactapi.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.contactapi.models.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {
    
    // custom query to search in contact
    List<Contact> findByFirstnameContaining(String text);  
}
