package com.example.contactapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.contactapi.models.Contact;
import com.example.contactapi.repositories.ContactRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * ExampleController
 * 
 * @author Daniel Padua
 */
@RestController
@RequestMapping("api")
public class ContactController {
    
    @Autowired
    private ContactRepository repository;

    @GetMapping("/contact")
    public List<Contact> index(){
        return repository.findAll();
    }

    @GetMapping("/contact/{id}")
    public Contact show(@PathVariable String id){
        int contactId = Integer.parseInt(id);
        return repository.findById(contactId).get();
    }

    @PostMapping("/contact/search")
    public List<Contact> search(@RequestBody Map<String, String> body){
        String searchTerm = body.get("text");
        return repository.findByFirstnameContaining(searchTerm);
    }

    @PostMapping("/contact")
    public Contact create(@RequestBody Map<String, String> body){
        String firstname = body.get("firstname");
        String lastname = body.get("lastname");
        String email = body.get("email");
        String tel = body.get("tel");
        return repository.save(new Contact(firstname, lastname, email, tel));
    }

    @PutMapping("/contact/{id}")
    public Contact update(@PathVariable String id, @RequestBody Map<String, String> body){
        int contactId = Integer.parseInt(id);
        // getting blog
        Contact contact = repository.findById(contactId).get();
        contact.setFirstname(body.get("firstname"));
        contact.setLastname(body.get("lastname"));
        contact.setEmail(body.get("email"));
        contact.setTel(body.get("tel"));
        return repository.save(contact);
    }

    @DeleteMapping("contact/{id}")
    public boolean delete(@PathVariable String id){
        int contactId = Integer.parseInt(id);
        repository.deleteById(contactId);
        return true;
    }
}
